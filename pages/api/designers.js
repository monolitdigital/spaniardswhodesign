import drive from "drive-db";

export default async (req, res) => {
  const db = await drive("1Uvk9k16Ys6PwLP4U_5vZutaWzYVGPWNIS7vTva8OFc8");
  let sanitizeResult = db.filter(
    (item) => item.name != "" && item.show == "Yes"
  );

  res.statusCode = 200;
  res.setHeader("Content-Type", "application/json");
  res.end(JSON.stringify(sanitizeResult));
};
